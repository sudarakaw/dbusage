// src/telegram.rs: wrapper module for required Telegram API
//
// Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use reqwest::blocking::Client;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize)]
struct TgMessage<'a> {
    chat_id: u32,
    text: &'a str,
    parse_mode: &'a str,
}

#[derive(Debug, Deserialize)]
pub struct Telegram {
    pub api_key: String,
    pub chat_id: u32,
}

impl Telegram {
    fn is_enabled(&self) -> bool {
        !self.api_key.is_empty() && 0 < self.chat_id
    }

    pub fn send(&self, text: &str) {
        if !self.is_enabled() {
            return;
        }

        let api_url = format!("https://api.telegram.org/bot{}", self.api_key);
        let msg = TgMessage {
            chat_id: self.chat_id,
            text,
            parse_mode: "markdown",
        };

        let cli = Client::new();
        let res = cli
            .post(format!("{}/sendMessage", api_url))
            .json(&msg)
            .send();

        match res {
            Ok(_) => return,
            Err(err) => eprintln!("{:?}", err),
        }
    }
}

impl Default for Telegram {
    fn default() -> Self {
        Telegram {
            api_key: String::from(""),
            chat_id: 0,
        }
    }
}
