// src/settings.rs: module for runtime settings
//
// Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use crate::{package, telegram::Telegram};
use config::{Config, ConfigError, Environment, File, FileFormat};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(default)]
pub struct Settings {
    pub device: String,
    pub threshold: u64,
    pub systemctl: Systemctl,
    pub telegram: Telegram,
}

impl Settings {
    pub fn from(file: &str) -> Self {
        Self::config(file)
            .map_err(|err| {
                println!("Failed to read settings: {}", err);
            })
            .unwrap_or_else(|_| Self::default())
    }

    fn config(file: &str) -> Result<Self, ConfigError> {
        let cfg = Config::builder()
            .add_source(
                File::with_name(&format!("/etc/{}rc", package::NAME))
                    .required(false)
                    .format(FileFormat::Toml),
            )
            .add_source(
                File::with_name(file)
                    .required(true)
                    .format(FileFormat::Toml),
            )
            .add_source(Environment::with_prefix("signatured").separator("_"))
            .build()?;

        cfg.try_deserialize()
    }
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            device: String::from("/dev/sde"),
            threshold: 100 * 1024 * 1024, // 100MB
            systemctl: Systemctl::default(),
            telegram: Telegram::default(),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct Systemctl {
    pub command: String,
    pub unit: String,
}

impl Default for Systemctl {
    fn default() -> Self {
        Systemctl {
            command: String::from("restart"),
            unit: String::from(package::NAME),
        }
    }
}
