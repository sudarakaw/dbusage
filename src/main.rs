// src/main.rs: main entry point for the dbusage program.
//
// Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

mod disk;
mod package;
mod settings;
mod telegram;

use std::process::Command;

use clap::Parser;
use disk::Storage;
use settings::Settings;

#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Args {
    #[clap(
        default_value = "/etc/dbusagerc",
        help = "Custom configuration file.",
        long,
        short,
        value_name = "FILE"
    )]
    config: String,
}

fn main() {
    let args = Args::parse();
    let settings = Settings::from(&args.config);
    let threshold = Storage::from_bytes(settings.threshold);
    let remaining_bytes = disk::remaining(&settings.device);

    match remaining_bytes {
        Ok(size) => {
            if size < threshold {
                // 🚨 - u+1F6A8
                let mut msg = format!(
                    "️️🚨 Only *{}* of device `{}` is remaining.",
                    size, settings.device
                );

                if settings.systemctl.command.is_empty() {
                    settings.telegram.send(&msg);
                } else {
                    msg = format!("{} Restarting service...", msg);
                    settings.telegram.send(&msg);

                    let result = Command::new("/usr/bin/systemctl")
                        .args([&settings.systemctl.command, &settings.systemctl.unit])
                        .output();

                    msg = format!(
                        "`systemctl {} {}`",
                        &settings.systemctl.command, &settings.systemctl.unit
                    );

                    match result {
                        Ok(output) => {
                            if output.status.success() {
                                // 🌻 - U+1F33B
                                msg = format!("🌻 {} *was Successful.*", msg);
                            } else {
                                // 👹 - U+1F479
                                msg = format!(
                                    "👹 {} *Failed!*\n```{}```\n```{}```",
                                    msg,
                                    String::from_utf8(output.stdout)
                                        .unwrap_or(String::from("-- stdout is empty --")),
                                    String::from_utf8(output.stderr)
                                        .unwrap_or(String::from("-- stderr is empty --"))
                                );
                            }
                        }
                        Err(err) => {
                            // 👹 - U+1F479
                            msg = format!("👹 {} *Failed!*\n```{:?}```", msg, err);
                        }
                    }

                    settings.telegram.send(&msg);
                }
            }
        }
        // 🛑 - U+1F6D1
        Err(msg) => settings.telegram.send(&format!("🛑 {}", msg)),
    }
}
