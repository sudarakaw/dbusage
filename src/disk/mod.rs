// src/disk/mod.rs: main module for disk related operations
//
// Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

mod storage;

use sysinfo::{DiskExt, System, SystemExt};

pub use storage::Storage;

pub fn remaining(device: &str) -> Result<Storage, String> {
    let mut sysinfo = System::new();

    sysinfo.refresh_disks_list();

    sysinfo
        // from list of all mounted storage devices
        .disks()
        .iter()
        // find the desiered device
        .find(|d| d.name() == device)
        // get amount of available space
        .map(|d| Storage::from_bytes(d.available_space()))
        .ok_or(format!("Unable to find the remaining space on {}", device))
}
