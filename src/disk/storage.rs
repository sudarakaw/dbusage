// src/disk/storage.rs: module to display and compare storage sizes
//
// Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use std::fmt::Display;

#[derive(Debug)]
pub enum Storage {
    Bytes(u64, f64),
    KB(u64, f64),
    MB(u64, f64),
    GB(u64, f64),
}

impl Storage {
    pub fn from_bytes(bytes: u64) -> Self {
        if bytes < 1024 {
            // less thn 1KB
            return Storage::Bytes(bytes, bytes as f64);
        }

        // size in Kilo Bytes
        let mut size = bytes as f64 / 1024.0;

        if size < 1024.0 {
            // less thn 1MB
            return Storage::KB(bytes, size);
        }

        // size in Mega Bytes
        size = size / 1024.0;

        if size < 1024.0 {
            // less thn 1GB
            return Storage::MB(bytes, size);
        }

        // size in Giga Bytes
        Storage::GB(bytes, size / 1024.0)
    }

    fn size(&self) -> &u64 {
        match self {
            Storage::Bytes(size, _) => size,
            Storage::KB(size, _) => size,
            Storage::MB(size, _) => size,
            Storage::GB(size, _) => size,
        }
    }
}

impl Display for Storage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Storage::Bytes(size, _) => write!(f, "{}B", size),
            Storage::KB(_, size) => write!(f, "{:.1}K", size),
            Storage::MB(_, size) => write!(f, "{:.1}M", size),
            Storage::GB(_, size) => write!(f, "{:.1}G", size),
        }
    }
}

impl PartialEq for Storage {
    fn eq(&self, other: &Self) -> bool {
        self.size() == other.size()
    }
}

impl PartialOrd for Storage {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.size().cmp(other.size()))
    }
}
