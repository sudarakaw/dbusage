# @configure_input@
#
# Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY;
# This is free software, and you are welcome to redistribute it and/or modify it
# under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

# Substitution variables
MAKEFILE_INCLUDES 	= @UNIQE_MAKEFILE_INCLUDES@
package 			= @PACKAGE_NAME@
srcdir 				= @srcdir@
sysconfdir 			= @sysconfdir@


# Default target - build everything buildable
.PHONY: all
all: build

include $(MAKEFILE_INCLUDES)


# Build target
.PHONY: build
build: $(BUILD_REGISTERED_TARGETS)


# Entry point to all tests of the application
.PHONY: check
check: build $(CHECK_REGISTERED_TARGETS)
	@echo "*** ALL TESTS PASSED ***"


# Install/Uninstall built application
.PHONY: install
install:
	$(INSTALL) -d $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) -m 0755 $(BUILDDIR)/$(package) $(DESTDIR)$(bindir)
	$(INSTALL) -d $(DESTDIR)$(libdir)/systemd/system
	$(INSTALL) -m 0644 $(srcdir)/systemd.service $(DESTDIR)$(libdir)/systemd/system/$(package).service
	$(INSTALL) -m 0644 $(srcdir)/systemd.timer $(DESTDIR)$(libdir)/systemd/system/$(package).timer
	$(INSTALL) -d $(DESTDIR)$(sysconfdir)
	$(INSTALL) -m 0644 $(srcdir)/rc $(DESTDIR)$(sysconfdir)/$(package)rc

.PHONY: uninstall
uninstall:
	$(RM) $(DESTDIR)$(bindir)/$(package)
	-rmdir $(DESTDIR)$(bindir) >/dev/null 2>&1

	$(RM) $(DESTDIR)$(libdir)/systemd/system/$(package).service
	$(RM) $(DESTDIR)$(libdir)/systemd/system/$(package).timer
	-rmdir $(DESTDIR)$(libdir)/systemd/system >/dev/null 2>&1
	-rmdir $(DESTDIR)$(libdir)/systemd >/dev/null 2>&1
	-rmdir $(DESTDIR)$(libdir) >/dev/null 2>&1
	$(RM) $(DESTDIR)$(sysconfdir)/$(package)rc
	-rmdir $(DESTDIR)$(sysconfdir) >/dev/null 2>&1


# Create source distribution archive
$(distdir): FORCE_DIST
	$(MKDIR_P) $@/
	$(CP) $(srcdir)/LICENSE $(distdir)
	$(CP) $(srcdir)/README.md $(distdir)
	$(CP) $(srcdir)/systemd.*.in $(distdir)
	$(CP) $(srcdir)/rc.in $(distdir)
	$(FIND) $(srcdir)/src/ -type f \
		-not -name "package.rs" \
		-exec $(CP) --parents {} $@/ \;


# Cleanup
.PHONY: clean
clean: $(CLEAN_REGISTERED_TARGETS)

.PHONY: distclean
distclean: clean $(DISTCLEAN_REGISTERED_TARGETS)
	$(RM) $(srcdir)/src/package.rs
	$(RM) $(srcdir)/systemd.service
	$(RM) $(srcdir)/systemd.timer
	$(RM) $(srcdir)/rc


# Self-rebuild
Makefile: Makefile.in config.status
	./config.status $@

config.status: configure
	./config.status --recheck
