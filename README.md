# dbusage

Application to watch MariaDB temporary disk usage, and restart service if space
is running out.

## License

Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.
